/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file   fatfs.c
  * @brief  Code for fatfs applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
#include "fatfs.h"

uint8_t retUSER;    /* Return value for USER */
char USERPath[4];   /* USER logical drive path */
FATFS USERFatFS;    /* File system object for USER logical drive */
FIL USERFile;       /* File object for USER */

/* USER CODE BEGIN Variables */

/* USER CODE END Variables */

void MX_FATFS_Init(void)
{
  /*## FatFS: Link the USER driver ###########################*/
  retUSER = FATFS_LinkDriver(&USER_Driver, USERPath);

  /* USER CODE BEGIN Init */
    /* additional user code for init */
}
#if 0
  /* USER CODE END Init */
}

/**
  * @brief  Gets Time from RTC
  * @param  None
  * @retval Time in DWORD
  */
DWORD get_fattime(void)
{
  /* USER CODE BEGIN get_fattime */
    return 0;
  /* USER CODE END get_fattime */
}

/* USER CODE BEGIN Application */
#endif
const char* ResultToStr(FRESULT res)
{
    switch (res)
    {
        case FR_OK:
            return "OK";
        case FR_DISK_ERR:
            return "Disk Error";
        case FR_INT_ERR:
            return "FATFS Internal Error";
        case FR_NOT_READY:
            return "Storage device not ready";
        case FR_NO_FILE:
            return "File not found";
        case FR_NO_PATH:
            return "Path not found";
        case FR_INVALID_NAME:
            return "Path name not valid";
        case FR_DENIED:
            return "Access denied";
        case FR_EXIST:
            return "Object already exists";
        case FR_INVALID_OBJECT:
            return "Invalid object";
        case FR_WRITE_PROTECTED:
            return "The media is write-protected";
        case FR_INVALID_DRIVE:
            return "Invalid drive number";
        case FR_NOT_ENABLED:
            return "Work area for the logical drive has not been mounted";
        case FR_NO_FILESYSTEM:
            return "No valid FAT volume found";
        case FR_MKFS_ABORTED:
            return "Format aborted";
        case FR_TIMEOUT:
            return "Timed out";
        case FR_LOCKED:
            return "Operation rejected by file sharing control";
        case FR_NOT_ENOUGH_CORE:
            return "Not enough memory";
        case FR_TOO_MANY_OPEN_FILES:
            return "Too many open files";
        case FR_INVALID_PARAMETER:
            return "Invalid Parameter";
        default:
            return "Unknown error";
    }
}
/* USER CODE END Application */
