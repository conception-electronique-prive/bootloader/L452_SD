#include <main.h>
#include <fatfs.h>
#include <spi.h>
#include <usart.h>
#include <gpio.h>
#include "app.h"

void print_info();
void deinit(void);

int main(void) {
    /* MCU Configuration--------------------------------------------------------*/
    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* Configure the system clock */
    SystemClock_Config();

    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_FATFS_Init();
    MX_USART1_UART_Init();
    MX_SPI3_Init();

    /* Infinite loop */
    print_info();
    if (Enter_Bootloader()) {
        print("Failed to prepare bootloader\r\n");
        Error_Handler();
    }

    if (Bootloader_CheckForApplication() == BL_OK) {
        print("Jumping to application\r\n");
        print(
          "\r\n"
          "--------------"
          "\r\n\r\n");
        deinit();
        Bootloader_JumpToApplication();
    }

    print("No application in flash.\r\n");
    Error_Handler();
}

void deinit(void) {
    MX_SPI3_DeInit();
    MX_USART1_UART_DeInit();
    MX_FATFS_DeInit();
    MX_GPIO_DeInit();
}

void print_info() {
    char       version[50];
    const char info[100] =
      "\033c"    // Clear Terminal Screen
      "CEP BOOTLOADER\r\n"
      "[BRAND]: %s\r\n"
      "[MODEL]: %s\r\n"
      "[COMM ]: %s\r\n"
      "[VER  ]: %s\r\n"
      "===================\r\n";
    char msg[100];
    Bootloader_GetVersion_Print(version, 50);
    snprintf(msg, 100, info, MCU_BRAND, MCU_MODEL, "SD", version);
    print(msg);
}
